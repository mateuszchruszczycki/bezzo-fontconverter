const Fontmin = require('fez-fontmin');

const fontmin = new Fontmin()
    .src('./fonts/**/*.ttf')
    .use(Fontmin.ttf2eot())
    .use(Fontmin.ttf2woff())
    .use(Fontmin.ttf2woff2())
    .use(Fontmin.ttf2svg())
    .dest('./build');

fontmin.run((err, files) => {
    if (err) {
        throw err;
    }
})
