# Bezzo FontConverter
1. `yarn` / `npm install`
2. Put `*.ttf` fonts in `fonts` folder like

```
fonts/
    OpenSans/
        *.ttf
```

3. `yarn start` /  `npm start`
4. All converted fonts are located in `build` folder
